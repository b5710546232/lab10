import ku.util.DummyStack;
import ku.util.Stack;
import ku.util.StackFactory;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 * @author nattapat sukpootanan.
 * @version 31/31/2015.
 * StackTest for test case of Stack.
 * 
 * */
public class StackTest {
	private Stack stack;
	/** "Before" method is run before each test. */
	@Before
	public void setUp( ) {
		int capacity = 3;
		//stack = new DummyStack( 2 );
//		StackFactory.setStackType(0);
		StackFactory.setStackType(1);
		stack = 	StackFactory.makeStack(capacity);
	}
	/**
	 * For testing new Stack is Empty.
	 * */
	@Test
	public void newStackIsEmpty() {
		assertTrue( stack.isEmpty() );
		assertFalse( stack.isFull() );
		assertEquals( 0, stack.size() );
	}
	/** pop() should throw an exception if stack is empty */
	@Test( expected=java.util.EmptyStackException.class )
	public void testPopEmptyStack() {
		Assume.assumeTrue( stack.isEmpty() );
		stack.pop();
		// this is unnecessary. For documentation only.
		fail("Pop empty stack should throw exception");
	}

	@Test
	/**
	 * peek() should be same all time that peek with out removing.
	 * */
	public void testPeek(){
		stack.push("1");
		stack.push("apple");
		assertSame(stack.peek(),stack.peek());
		assertSame(stack.peek(),"apple");
	}
	/**
	 * push() should throw an exception if stack is Full. 
	 * */
	@Test( expected=IllegalStateException.class)
	public void testPushFull(){
		stack.push("apple");
		stack.push("banana");
		stack.push("pineapple");
		stack.push("coconuts");
		stack.push("grape");
	}
	/**
	 * size() when add object in stack size should be update same number of object
	 * unlil It is full. It will be equals capacity(maximum size).
	 * */
	@Test
	public void testSize(){
		assertSame(stack.size(),0);
		stack.push("1");
		assertSame(stack.size(),1);
		stack.push("2");
		assertSame(stack.size(),2);
		stack.push("3");
		
	}
	/**
	 *Tesing pop() of stack it can pop.
	 * */
	@Test 
	public void testPopStack(){
		stack.push("1");
		stack.push("2");
		assertSame(stack.pop(),"2");
		assertSame(stack.pop(),"1");
	}
	/**
	 * Testing Stack is Full.
	 * */
	@Test
	public void testIsFull(){
		assertFalse(stack.isFull());
		stack.push("1");
		assertFalse(stack.isFull());
		stack.push("2");
		stack.push("3");
		assertTrue(stack.isFull());
		
	}
	/**
	 * push() should be throw exception if push null.
	 * */
	@Test( expected=IllegalArgumentException.class )
	public void testPushNull(){
		stack.push(null);
	}
	/**
	 * Capacity should be return maximum size of Stack.
	 * */
	@Test
	public void testCapacity(){
		stack.push("1");
		assertSame(stack.capacity(),3);
		stack.push("2");
		assertSame(stack.capacity(),3);
		stack.push("3");
		assertSame(stack.capacity(),3);
		stack.push("4");
		assertSame(stack.capacity(),3);
	}
	/**
	 * Testing is Empty if It is nothing in Stack.
	 * */
	@Test 
	public void testIsEmpty(){
		assertTrue(stack.isEmpty());
		stack.push("1");
		assertFalse(stack.isEmpty());
		stack.push("2");
		assertFalse(stack.isEmpty());
	}
	
	

}